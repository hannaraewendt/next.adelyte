---
title: "Part 4a: How to Build the Best Home Theater for $30,000—Or “That Budget Sucks”"
date:  2017-02-07
author: Johnnie Sanchez 
image: images/blog/survey.png
type : "page"
---
Ken Forsythe of Screen Innovations and Robert Keeler of Meridian Audio created their [dream home theater](https://adelyte.com/blog/part-4-how-to-build-the-best-home-theater-for-30000-or-that-budget-sucks) on our blog last month. They were given a strict $30,000 budget, and both worked within it. At the end of the blog we asked our readers to vote on their favorite theater. [Meridian Audio](https://www.meridian-audio.com/) won in a landslide, receiving 85% of the vote, with [Screen Innovations](https://www.screeninnovations.com/) obtaining just 15% of it. One of the most noticeable differences between the two theaters was that Meridian Audio choose a LG Flat Panel 65EF9500 65” OLED Flat Panel, while Screen Innovations, not surprisingly, kept it old school by putting their trust in Sony as their projector provider. 

