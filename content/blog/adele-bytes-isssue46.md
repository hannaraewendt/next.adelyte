---
title: "Adelyte Bytes: Issue 46"
date:  2017-01-22
author: Johnnie Sanchez 
image: images/blog/harman-connected-pa-0117.jpg
type : "page"
---

## Harman Introduces [Connected PA Ecosystem](http://www.ravepubs.com/harman-introduces-harman-connected-pa-ecosystem-interesting/)

Harman Professional Solutions announced the release of the HARMAN Connected PA, a complete ecosystem of live sound products that will allow performers at any skill level to get professional results. The Connected PA streamlines the setup process with just one state-of-the-art app that removes the need for multiple app management. This system will be available later this year.




## Did the US Army Take Inspiration from Star Wars for this [Hoverbike](http://mashable.com/2017/01/20/us-army-hoverbike/?utm_campaign=Mash-Prod-RSS-Feedburner-All-Partial&utm_cid=Mash-Prod-RSS-Feedburner-All-Partial#3FlepsogfSqM)?

The United States Army has joined forces with the United Kingdom company Malloy Aeronautics to build an electric quadcopter that will have the ability to support over three hundred pounds.


## Brink’s Brand Returns With [“Smart Lock”](http://www.cepro.com/article/brinks_is_back_with_new_249_smart_lock) for Hands-Free Doorknobs

In 2008, the company Brink’s Home Security was sold to Tyco, but the company Hampton Products kept the rights to the Brink’s name. However, Brink’s has been out of the world of electronic security for about a decade. This changed at the recent International Builders Show: Hampton introduced Brink’s ARRAY, a smart deadbolt lock that comes with Amazon Echo control and fast installation time. The lock is priced at two hundred and forty-nine dollars and is currently scheduled for a tentative summer release date. Hampton hopes to sell the lock online and in stores like Home Depot.


## Jesse Walsh Joins [Primeview Americas](http://www.avnation.tv/press-release/jesse-walsh-joins-primeview-americas-as-business-development-manager/)

Primeview is a company that has made a name for itself in the world of display technology as a global leader. The company, which was launched in 1997, manufactures on the most advanced ISO-certified production lines and has solutions featured in properties and businesses around the world. The company plans to take Jesse Walsh, a West Coast sales representative from Pacific Coast Visions in California, on as a Business Development Manager. Jesse has expressed excitement with his new position, saying that he is looking forward “to be working with Primeview, the world’s preeminent manufacturer of commercial video display solutions.”                                                                                 

## What is [Meitu] and Why Are People On Edge About It?

Meitu is a Chinese photo filter app that has recently taken the United States by storm. The iOS and Android app essentially transforms selfies into anime-styled caricatures. However, the app demands a lot of permissions, including access to your phone’s GPS and other data that has been putting security researchers on edge. They have questions regarding what Meitu intends to do with all this data, given that the app doesn’t need it to make your face appear sparkly and cartoony. Meitu’s website released a statement that says that the data is used to “optimize app performance”. Make of that what you will, but users have other complaints, like that the app makes people of all skin colors appear much whiter.