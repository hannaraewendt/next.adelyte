---
title: "Adelyte Bytes: Issue 49"
date:  2017-02-12
author: Johnnie Sanchez 
image: images/blog/leedarson_arrival_sensor_ise2017.jpg
type : "page"
---

## Leedarson’s Arrival Sensor: The Most Interesting Little [Z-Wave](http://www.cepro.com/article/leedarsons_arrival_sensor_the_most_interesting_little_z_wave_device_at_ise) Device at ISE 2017

Leedarson’s Arrival Sensor was a winning product at ISE 2017. The device tells the smart-home system if users are coming or going. Once this information is received, the system can turn to ‘away’ or unlock the door, depending on what the user is doing. It is designed so that users do not have to press any button in order to unlock the door or turn on the alarm.

The system pings a Z-Wave system, using this instead of Bluetooth, and lets the device know that the person is in the house. There is also a built-in accelerometer, which maintains battery life when the device is not doing anything. This device is designed to be enrolled into only one pre-existing control system.




## [Project Rome SDK](http://www.digitaltrends.com/computing/microsoft-project-rome-android-sdk/?utm_source=feedly&utm_medium=webfeeds) is the Latest Step Toward Android-Windows 10 Integration

Microsoft has been expanding their compatibility from only Windows 10 to Android and iOS in the past few years. At the Build 2016 event, they announced Project Rome, which is another cross-platform project, connecting Android devices to Windows 10 machines. The purpose of this project is to make Windows 10 not a compromise, and allow it to fit in with users’ lives. While they don’t have all of the required functionality yet for Project Rome, the Windows 10 components needed for this project were released during the Windows 10 Anniversary Update earlier this year.

An Android version of Project Rome software development kit, or SDK, is available for Java and Xamarin. The app can communicate across Android and Windows 10 UWP platforms. The needed tools for this project is planned to be included in an upcoming release of Android SDK. GitHub contains Java and Zamarin examples for developers, for the time being.

 

## These Boots Keep [Astronauts](https://www.wired.com/2017/02/boots-keep-astronauts-tripping-feet/) From Tripping Over Their Own Feet

Alison Gibson, a graduate researcher at MIT, is creating a new kind of space boot for astronauts. This boot is intended to reduce the amount of falling that occurs during moon walks as well as decrease the amount of looking down. It is dangerous for astronauts to be falling all the time in case they land on something that could compromise their space suit. This is especially important for astronauts on Mars, where gravity is twice as strong and the terrain is much rockier than the moon. Although NASA doesn’t expect to even put humans in orbit around Mars until the 2030s, it is important anyways for these boots to be manufactured, as they have been shown to significantly reduce the amount of time that astronauts spend looking down.

Currently, astronauts only wear soft boots in space, as they only spend time in the International Space Station, and not walking on the moon. This means they don’t have to wear the huge boots that are rigid starting at the knee. However, these are non-ideal. The helmets the astronauts must wear significantly reduce peripheral vision and pressurized boots can’t feel for obstacles, forcing astronauts to look down to ensure they won’t run into anything. Gibson’s boots are designed with two vibrating motors that send a vibration to the big toe when they are approaching an obstacle, the intensity increasing as the object gets closer. Additionally, orange dots appear on the boot’s visual display, becoming more and more opaque as the object approaches. Studies have proven this feature successful, as it significantly cuts down on the amount of time spent looking down.


## Skylanders Skips a [2017 Console Release](http://www.digitaltrends.com/gaming/skylanders-skips-2017-console-release/?utm_source=feedly&utm_medium=webfeeds), Refocuses on Mobile Instead

Skylanders is not releasing a new console this year, something they haven’t done since their initial release in 2011. Skylanders is a series by Activision, which is a part of the toys-to-life market. This industry has been dwindling recently, and Skylanders suffered low sales in 2016, possibly leading to the decision to not release a new console in 2017. Skylanders still plans to roll out new content, but this is designed for 2016’s Skylanders Imaginators. This news is coming after a multitude of cancellations from Disney Interactive Studios. Skylanders is ensuring consumers that it is not cancelling the series, only focusing on mobile gaming for this upcoming year.