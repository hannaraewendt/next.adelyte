---
title: "Adelyte Bytes: Issue 50"
date: 2017-02-19
author: Johnnie Sanchez 
image: images/blog/apple_tv_4k.jpg
type : "page"
---

## So, Barbie’s a [Hologram](https://www.wired.com/2017/02/hello-barbie-hologram-matell/) Now

Barbie has traded in plastic for hologram. Called the Hello Barbie Hologram, she is now an animated projection within a small box. A screen on the top of the box projects the image onto a translucent wedge, and creates an image of her floating. However, in order to receive the 3-D effect, one must look at her from straight on. She is not 3-D from the sides and from the back is completely invisible. The activation phrase, as the name of the toy would suggest, is “Hello Barbie.” She responds to more voice commands than just that, though. She has the ability to change clothes or appearance, set alarms or reminders, and many other features.

There have been some privacy concerns, particularly because children are involved. However, the company stated that they have a Holo-box that uses a 256-bit encryption to send data to a cloud system in order to follow the Children’s Online Privacy Protection Rule. They have also stated that they do not save any recordings to a server.

The only prototype of Hello Barbie is at the American International Toy Fair in New York, and is still only partially completed. However, the company is optimistic and anticipates a cost under $300.


## Amazon Alexa Voice Control Live for [URC Total Control](http://www.ravepubs.com/amazon-alexa-voice-control-live-urc-total-control/)

Amazon Alexa Smart Home Skill integration for URC Total Control has been recently released. This system is designed to connect with multiple Alexa-enabled devices, such as Amazon Echo. The integration is meant to completely combine Alexa devices with Total Control, giving users the ability to give a variety of voice commands with their smart home system. These commands can range from choosing a movie to adjusting the temperature. This new system is not only for new voice control users but also pre-existing ones.


## Move Over Roku, [4K Apple TV](http://www.cepro.com/article/roku_4k_apple_tv_testing) in Testing

Apple has reportedly been testing its fifth-generation 4K Apple TV. This is coming after a year of disappointing sales of Apple’s fourth-generation 4K Apple TV, which the company had hoped would be their grand entrance into the industry. Apple has been attempting to break into the TV industry for a while, something Steve Jobs reportedly had envisioned for his company. Apple is now competing with the likes of Amazon and Roku, although they claim that their 4K TV is capable of the same functionalities. It is anticipated to come out later this year. The TV will include more vivid colors, and perhaps HDR support as well.



## Target Seriously Upgrades Its [IoT](http://www.cepro.com/article/target_upgrades_iot_retail_presence) Retail Presence

Target recently completely a large overhaul of its Target Open House store located in San Francisco, opening on February 10 after a seven week long remodel. This new store includes an area called Garage. Here, 16 connected home products each months are showcased, and Target customers can give feedback. This store also boasts a new event space, meant to easily transition from daytime store to nighttime activity center. This space includes touchscreen monitors and approximately 70 IoT (Internet of Things) products. The newly remodeled store also includes a more personalized experience, where companies can demonstrate their various products and receive feedback and answer questions.