---
title: "Adelyte Bytes: Issue 48"
date:  2017-02-05
author: Johnnie Sanchez 
image: images/blog/samsung_smart_tv_no_video_for_you_w.jpg
type : "page"
---

## Key Feature Update for [Solstice](https://www.avnation.tv/press-release/ise-2017-key-feature-update-for-solstice/)

Mersive Solstice has announced that its newest update, Solstice 3.0, is ready for multi-room use. This means that users don't have to be in the same room in order to share content. The new update includes all current features, but allows them to be used in different locations.

One important feature of this update is that no hardware change or upgrade is necessary in order to use multi-room. Customers with the current software will be able to update on their current Solstice Enterprise Edition Pods and have access to the multi-room feature. The multi-room update is set to be introduced at Integrated Systems Europe, which takes place February 7-10 in Amsterdam. Support for Solstice Windows Software is expected sometime in mid-2017.


## HDBaseT Snafu? Don’t Update [Samsung TV's](http://www.cepro.com/article/warning_dont_update_samsung_tvs_hdbaset_snafu)

Although an official announcement has yet to be released, it has been reported that Samsung’s update for Internet-connected TVs is faulty. Apparently Samsung’s TV is unable to communicate through the HDBaseT. This has caused HDMI sources that are connected to HDBaseT unable to be played. Effectively, the TV's have stopped playing.

A warning was originally tweeted by UK’s #LiveInstall, and reports have since confirmed their statement. It is still early on in the reports, however, and Samsung has not yet released any statement or comment on the issue. It is thus difficult to determine who has been affected by the issues. The most common report is that issues arise when the TV is in 4K mode, and people have been advising users to switch to 1080p for the time being.

It is estimated to be an HDCP timing issue, although the cause is still unknown. Advice says to shut off auto-update on web-connected Samsung displays, and it seems to be particularly affecting the 6000, 7000, and 8000 series.




## VITEC Brings IPTV & Signage Sports Venue Solution to [National Sports Forum](https://www.vitec.com/company/news-press-releases/article/vitec-brings-iptv-signage-sports-venue-solution-to-national-sports-forum/)

VITEC is set to introduce its EZ TV 8.0 IPTV at the National Sports Forum. VITEC’s EZ TV 8.0 has many new and useful features, including an integrated IPTV and digital signage platform. It is designed specifically for sports stadiums or arenas, allowing high-quality live streaming and on-demand video. It can be used on current IP infrastructures, which makes it flexible for usage in almost all venues, without the need for any large installments.

Their product includes low-latency playback and real-time updates to the electronic program guide. It has a time-shifted TV and allows for live video access from PCs and mobile devices. There is also a feature allowing for the user to view multiple channels at once, which is an important feature for sports arenas that wish to give the audience access to all points of view. It is designed to include uncomplicated signage authoring, administration and analytics, so that it can easily be incorporated into any stadium.

VITEC’s IPTV and signage end-points contain separate hardware processes for media, allow for highly complex digital signals. It can blend live streams, video files, social networks and current data such as scores and menus. The National Sports Forum where this will be showcased will take place at the Minneapolis Convention Center on February 12-14. The National Sports Forum is the biggest gathering of team sports marketing, sales, promotions, and event entertainment executives and they hail from many different teams and leagues all throughout North America.

