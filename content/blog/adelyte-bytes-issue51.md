---
title: "Adelyte Bytes: Issue 51"
date: 2017-02-28
author: Johnnie Sanchez 
image: images/blog/brookfield_home_apple_wide.jpg
type : "page"
---

## Builder to Push [Apple HomeKit](http://www.cepro.com/article/builder_to_push_apple_homekit_home_automation_to_47000_homes) Home Automation to 47,000 Homes

Home builder Brookfield Residential is hoping to install Apple HomeKit in thousands of homes in Ontario Ranch within the next 20 years. They plan on expanding from their current 1,000 homes to 47,000 in the small Los Angeles community. These homes are approximately 3,500 square feet and are priced in the $500,000's.

The Apple HomeKit includes Lutron Caseta lighting systems, Lutron Serena shades and Honeywell Lyric thermostats. Homeowners can choose the package they want for their homes, which will be installed before they move in by Brookfield Residential. These homes are designed to support voice control and to connect with iOS 10. It is now possible to control all smart devices without using individual apps for each, which was one of the biggest issues with Apple HomeKit. These homes will additionally include LED lighting, high-speed Ethernet wiring in multiple rooms, USB outlets in the kitchen, as well as multi-zone heating and cooling.



 

## Ambient [Light Rejecting](http://www.cepro.com/article/ambient_light_rejecting_screens_and_the_laws_of_physics) Screens and the Laws of Physics

Ambient Light Rejection, often shortened to ALR, is a screen designed for the purpose of blocking out outside lighting from overhead lights or windows while still providing bright, high-quality pictures. While normal surfaces typically reflect light evenly, ALR instead uses a combination of optical filters and dispersion. It combines the technologies of projection screens and angular reflective screens to achieve the ideal viewing image. The projection screen uses a matte white to reflect all light at wide angles, although it also diffuses ambient light which ALR is attempting to avoid. Angular reflective screens reflect all light in one direction, which allows incoming light to be managed. ALR screens contain multiple layers, which filter away incoming light while reflecting the desired image, creating some viewing angles which are superior to others. The current leader in ALR screens is Stewart Filmscreen Phantom HALR.

 

## Pliant Technologies Displays Lightweight Professional Communication Headset, SmartBoom LITE, at [NAB 2017](http://www.ravepubs.com/pliant-technologies-displays-lightweight-professional-communication-headset-smartboom-lite-nab-2017/)

Pliant Technologies showed off their SmartBoom LITE headset at NAB 2017. This headset is designed with comfort and flexibility in mind, made as small and lightweight as possible. It includes high audio quality, design upgrades and an adjustable microphone boom that can quickly mute. It also includes an ambidextrous swiveling microphone boom as well as an adjustable headband and replaceable ear pad. For better audio quality and noise cancellation, there is a closed back on-ear design, single-ear form factor, and cardioid microphone.

 

## Inside the [Extreme Machine](https://www.wired.com/2017/02/inside-extreme-machine-mimics-bombs-black-holes/) that Mimics Bombs and Black Holes

Sandia National Laboratories, located in Albuquerque, is owned by the Department of Energy’s National Nuclear Security Administration. Its main purpose is to study nuclear weapons as well as the nature of the universe, which it accomplishes at once with the z-machine. The z-machine mimics the conditions of a nuclear bomb detonation, fusion reactor, or the center of a star in order to study what happens to matter in these extreme environments. The machine is composed of 2,160 capacitors filled with electricity, surrounding a 104-foot-wide wheel, all placed inside of a tank filled either with oil or water depending on the aim of the experiment. When activated, the capacitors release an enormous amount of electricity, sending it to the cables inside of the liquid. They are aimed at the wheel’s hub, which is placed inside of a 20-foot tall vacuum cylinder. After each experiment, this is destroyed, meaning they have to build a new cylinder after each detonation, which occurs approximately 150 times per year.

 

 
