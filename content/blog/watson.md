---
title: "Watson Doesn't Need Sherlock"
date: 2017-02-15
author: Johnnie Sanchez 
image: images/blog/network-782707_960_720.png
type : "page"
---

The audiovisual industry continues to evolve with exciting news of new technology and acquisitions happening often. One of the exciting merges of technology was Harman's partnership with IBM. Harman [partnered](http://pro.harman.com/insights/enterprise/harman-and-ibm-pioneering-voice-enabled-cognitive-rooms/) with IBM to integrate connectivity solutions with Watson, IBM's innovative cognitive technology. Together, the two companies have enabled connectivity applications for hotel rooms, hospital rooms, and conference rooms. Using IoT solutions, Harman and IBM are providing connected lifestyles and solutions.


IBM is showing themselves to be the experts of IoT with their [new headquarters](http://www.thedrum.com/news/2017/02/24/welcome-ibm-watson-s-internet-things-hq-where-the-office-ecosystem-and-ai-augmented) in Munich, Germany. These headquarters are IBM's first outside of the United States and is one of the largest investments that they have made in years. The building is comprised of 25 floors of collaborative workspaces for IBM and their partners.


Throughout the building are functioning IoT devices as well as some prototypes thrown in to make the space as connective as possible. Many of the workstations are open and unassigned allowing the integrated technology to confirm which desks are readily available for new employees. Lights help guide new employees and visitors to their desks and Watson IoT automatically adjusts the temperature and lighting based upon the user's preference. IBM's innovative thinking and solutions are helping many of their partners achieve their collaborative dreams in the workplace.