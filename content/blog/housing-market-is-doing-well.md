---
title: "Housing Market Is Doing Well"
date: 2017-02-26
author: Johnnie Sanchez 
image: images/blog/house-1429409_960_720.png
type : "page"
---



Despite the rise in U.S. mortgage rates recently, retail companies such as Home Depot and Lowe's continue to do well. Their success is a combination of new home construction and home remodeling. One reason for the success of these home improvement stores is because a contractor can't purchase their products via [Amazon](https://www.thestreet.com/story/14020897/1/this-is-why-lowe-s-and-home-depot-are-blowing-everyone-away-in-crumbling-retail-sector.html) and have it delivered to a job site- at least not yet.


As successful as the box companies are with home improvements, some home buyers are not having the same success. Higher home prices are causing potential home buyers to rent as that provides them with a better financial option then purchasing a house that they are [unable to afford.](http://www.cnbc.com/2017/02/28/spring-housing-already-overheating-think-60-offers-on-one-house.html) Other potential home buyers that are finding the market to be too competitive are choosing to remodel their existing living space, allowing it to become a more functional or comfortable space that better fits their needs.


In January, [Forbes](https://www.forbes.com/sites/samanthasharf/2017/01/03/housing-outlook-2017-eight-predictions-from-the-experts/#200348547fa0) provided their housing predictions for 2017, which includes a steady increase in housing prices and sellers maintaining an edge over buyers as the supply of house for buyers will improve, but still remain competitive.


If you are a potential home buyer, what can you do? The most important thing is research. Determine the area you want to live in, the style of housing, and amount of property you desire. You also need to determine how much you can afford. While it may feel amazing to purchase your dream home, 3 months of eating ramen noodles at every meal makes the ‘amazing’ feeling wear off as the despair of your overstretched income kicks in. What if you find yourself at the point where you can't comfortably purchase a new house yet? Stay where you are and remodel- remodeling can be an affordable option and transform your existing space into your dream home.

 

