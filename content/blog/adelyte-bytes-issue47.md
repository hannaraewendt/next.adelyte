---
title: "Adelyte Bytes: Issue 47"
date:  2017-01-30
author: Johnnie Sanchez 
image: images/blog/cedia_show_sold_w-1.jpg
type : "page"
---

## CEDIA [Sells](http://www.cepro.com/article/cedia_sells_ownership_trade_show) Ownership of Trade Show

CEDIA has sold the rights to the CEDIA Expo to Emerald Expositions for an undisclosed amount of money. Emerald Expositions is an operator of business-to-business trade shows in the United States. Vin Bruno, the CEO of CEDIA, says that Emerald Expositions approached CEDIA with an offer shortly after the most recent CEDIA Expo in Dallas. 2017 is expected to be a transitional year for CEDIA as the company begins to work alongside Emerald Expositions. Beyond 2017, Emerald Expositions will be responsible for handling all operational aspects of the show, including the CEDIA Awards event.




## Could [Robots](http://mashable.com/2017/01/27/robots-emotional-support-love-life/?utm_campaign=Mash-Prod-RSS-Feedburner-All-Partial&utm_cid=Mash-Prod-RSS-Feedburner-All-Partial#WI.aMxS1AaqD) Provide Emotional Support in Your Love Life?

A faceless robot named Travis was used in a study on emotional support. Based on the results, it is entirely possible that robots will one day be used to provide emotional support for people, much like friends, family, and pets currently do.


## FORTRESS Seating Debuts [Smart Chair](http://www.ravepubs.com/fortress-seating-debuts-smart-chair-technology-emphasizes-customization-ise-2017/) Technology

FORTRESS Seating is a leader in the world of home cinema, media room, and contract seating. Their latest product, the Smart Chair technology, will make an international debut during Integrated Systems Europe 2017, which will be held in Amsterdam RAI in early February. It will be on display in the Home Cinema Europe stand. The new technology will emphasize quality and customization.


## [Self-Driving](http://www.recode.net/2017/1/27/14406702/self-driving-cars-autonomous-vehicles-uber-lyft-tesla-waymo-recode-podcast) Cars: When, Oh When, Will They be Available?

Recode’s Johana Bhuiyan had a possible prediction on the latest episode of Too Embarrassed To Ask. According to her, if people are expecting self-driving cars via companies like Uber and Lyft, it should happen “a lot faster than we expect it to”. If car ownership continues and people stop using the services of Uber and Lyft, the release of self-driving cars should take longer. Currently, an issue facing self-driving cars is whether or not drivers should be required to keep their hands on the steering wheel when the car is in motion.

